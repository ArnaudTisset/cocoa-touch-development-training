//
//  JLGSimpleBrowserViewController.m
//  SimpleBrowser
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import "JLGSimpleBrowserViewController.h"
#import "JLGTableViewController.h"

@interface JLGSimpleBrowserViewController ()
- (IBAction)gotoUrl:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *requestIndicator;
@property NSString *currentAddress;
@end

@implementation JLGSimpleBrowserViewController

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    JLGTableViewController *tableViewController = [segue destinationViewController];
    
    
    NSLog(@"tableview./history %@", tableViewController.history);
    
    
    if(tableViewController.history == nil)
    {
        tableViewController.history = [[NSMutableArray alloc] init];
    }
    
    for(int i=0; i < [_history count]; i++)
    {
        [tableViewController.history addObject:[_history objectAtIndex:i]];
    }
    
    NSLog(@"Segue tableView Controller History %@", tableViewController.history);
    
    
    // cont.history
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _webView.delegate = self;
    
    NSString *address = @"http://google.com/";
    _currentAddress = address;
    NSURL *url = [NSURL URLWithString:address];
    
    
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    _requestIndicator.hidesWhenStopped = TRUE;
    
    _history = [[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoUrl:(id)sender {
    _currentAddress = _urlTextField.text;
    NSURL *url = [NSURL URLWithString:_currentAddress];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [_requestIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_requestIndicator stopAnimating];
    
    NSMutableArray * newHistory = [[NSMutableArray alloc] init];
    
    [newHistory addObject:_currentAddress];
    // NSLog(@"current address %@", _currentAddress);
    
    for(int i = 1; i < 3; i++)
    {
        [newHistory addObject:[_history objectAtIndex:i-1]];
    }
     // NSLog(@"new history: %@",newHistory);
    
    for(int i=0; i < [newHistory count]; i++)
    {
        _history[i] = newHistory[i];
    }
    // NSLog(@"history: %@", _history);
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Erreur" message:@"L'url saisie est incorrecte" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView                           :(NSInteger)buttonIndex
{
    
}
@end
