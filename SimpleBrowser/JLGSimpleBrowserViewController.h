//
//  JLGSimpleBrowserViewController.h
//  SimpleBrowser
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JLGSimpleBrowserViewController : UIViewController <UIWebViewDelegate>
@property NSMutableArray *history;

@end
