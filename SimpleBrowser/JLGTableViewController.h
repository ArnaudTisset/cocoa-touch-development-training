//
//  JLGTableViewController.h
//  SimpleBrowser
//
//  Created by Jlformation on 6/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JLGTableViewController : UITableViewController
@property NSMutableArray *history;
@end
