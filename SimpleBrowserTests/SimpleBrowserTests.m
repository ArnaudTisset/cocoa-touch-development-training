//
//  SimpleBrowserTests.m
//  SimpleBrowserTests
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SimpleBrowserTests : XCTestCase

@end

@implementation SimpleBrowserTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    // XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
    NSString *strings[3];
    strings[0] = @"First";
    strings[1] = @"Second";
    strings[2] = @"Third";
    
    NSArray *stringsArray = [NSArray arrayWithObjects:strings count:2];
    XCTAssertTrue([stringsArray count] == 2);
}

- (void)testExample2
{
    // XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
    NSString *strings[3];
    strings[0] = @"First";
    strings[1] = @"Second";
    strings[2] = @"Third";
    
    NSArray *stringsArray = [NSArray arrayWithObjects:strings count:2];
    XCTAssertTrue([stringsArray count] == 2);
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
